package help

import (
	"math/rand"
	"time"
	"github.com/dgrijalva/jwt-go"
	eu "entities/user"
)

func GenerateRandomString() (string) {
	r := rand.New(rand.NewSource(time.Now().UnixNano() % int64(1000)))
	length := 45
	runes := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, length)
	for i := range b {
		b[i] = runes[r.Intn(length)]
	}
	return string(b)
}

func GenerateRandomCode() (string) {
	r := rand.New(rand.NewSource(time.Now().UnixNano() % int64(1000)))
	length := 6
	runes := []rune("0123456789")
	b := make([]rune, length)
	for i := range b {
		b[i] = runes[r.Intn(length)]
	}
	return string(b)
}

func GenerateJwtTokens(us *eu.User) (string, string) {
	now := time.Now()

	accToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id": us.Id.Value,
		"exp": now.Add(time.Hour * 24 * 7).Unix(),
	})
	accString, err := accToken.SignedString([]byte(us.Secret))
	if err != nil {
		panic(err)
	}

	refToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id": us.Id.Value,
		"exp": now.Add(time.Hour * 24 * 30).Unix(),
	})
	refString, _ := refToken.SignedString([]byte(us.Secret))

	return accString, refString
}

func Contains(slice []*eu.Id, elem *eu.Id) (bool) {
	for _, el := range slice {
		if el.Value == elem.Value {
			return true
		}
	}
	return false
}

func Remove(slice *[]*eu.Id, elem *eu.Id) {
	for i, rCount, rLen := 0, 0, len(*slice); i < rLen; i++ {
		j := i - rCount
		if (*slice)[j].Value == elem.Value {
			*slice = append((*slice)[:j], (*slice)[j+1:]...)
			rCount++
		}
	}
}

func UserIdsWithoutId(ids []*eu.Id, userId *eu.Id) ([]*eu.Id) {
	var array []*eu.Id
	for _, id := range ids {
		if id.Value != userId.Value {
			array = append(array, id)
		}
	}
	return array
}