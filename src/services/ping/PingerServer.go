package ping

import (
	"context"
	"services/ping/proto"
)

const Port = ":228"

type Server struct {}

func (cs *Server) Get(c context.Context, ping *ping_proto.Ping) (*ping_proto.Pong, error) {
	return &ping_proto.Pong{ Text: "Pong Pong Pong" }, nil
}