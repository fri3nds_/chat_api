package auth

import (
	"context"
	ap "services/auth/proto"
	ca "centers/auth"
	fp "factories/phone"
	"help"
	cv "centers/validate"
	fr "factories/response"
)

type Server struct {
	authCenter *ca.AuthCenter
	validator  *cv.ValidateCenter
	factory    *fr.AuthFactory
}

// static

func New(validator *cv.ValidateCenter) (*Server) {
	authCenter, err := ca.New()
	if err != nil {
		panic(err)
	}
	factory := &fr.AuthFactory {}
	return &Server { authCenter: authCenter, validator: validator, factory: factory }
}

// public rpc

func (as *Server) Login(c context.Context, phone *ap.Phone) (*ap.Result, error) {
	as.authCenter.Login(fp.ConvertPhone(phone))
	return &ap.Result{ Value: 1 }, nil
}

func (as *Server) Verify(c context.Context, req *ap.VerifyReq) (*ap.VerifyResp, error) {
	if us := as.authCenter.Verify(fp.ConvertPhone(req.Phone), req.Code.Value); us != nil {
		accToken, refToken := help.GenerateJwtTokens(us)
		return as.factory.VerifyResp(1, us, accToken, refToken), nil
	}

	return as.factory.VerifyResp(0, nil, "", ""), nil
}

func (as *Server) LoginWithoutPhone(c context.Context, req *ap.LoginWithoutPhoneReq) (*ap.LoginWithoutPhoneResp, error) {
	if us := as.authCenter.LoginWithoutPhone(req.Username, req.Password); us != nil {
		accToken, refToken := help.GenerateJwtTokens(us)
		return as.factory.LoginWithoutPhone(1, us, accToken, refToken), nil
	}

	return as.factory.LoginWithoutPhone(0, nil, "", ""), nil
}

func (as *Server) Register(c context.Context, req *ap.RegisterReq) (*ap.RegisterResp, error) {
	if as.authCenter.Register(req.Username, req.Password) {
		return as.factory.Register(1), nil
	}

	return as.factory.Register(0), nil
}

func (as *Server) Logout(c context.Context, req *ap.LogoutReq) (*ap.LogoutResp, error) {
	if !as.validator.ValidateJwt(req.AccToken) {
		return as.factory.LogoutResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if as.authCenter.Logout(req.DevToken) {
		return as.factory.LogoutResp(1, help.StatusCode_OK), nil
	}
	return as.factory.LogoutResp(0, help.StatusCode_OK), nil
}

func (as *Server) Refresh(c context.Context, req *ap.RefReq) (*ap.RefResp, error) {
	if !as.validator.ValidateJwt(req.RefToken) {
		return as.factory.RefResp(0, help.StatusCode_FORBIDDEN, "", ""), nil
	}

	id, _ := as.validator.ValidateJwtId(req.RefToken)
	accToken, refToken := as.authCenter.Refresh(id)

	return as.factory.RefResp(1, help.StatusCode_OK, accToken, refToken), nil
}