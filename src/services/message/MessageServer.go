package message

import (
	cv "centers/validate"
	"context"
	mp "services/message/proto"
	cm "centers/message"
	fr "factories/response"
	"help"
	fm "factories/message"
	fc "factories/chat"
)

type Server struct {
	messageCenter *cm.MessageCenter
	validator *cv.ValidateCenter
	factory *fr.MessageFactory
}

// static

func New(validator *cv.ValidateCenter) (*Server) {
	messageCenter, err := cm.New()
	if err != nil {
		panic(err)
	}
	factory := &fr.MessageFactory {}
	return &Server{ messageCenter, validator, factory }
}

// public rpc

func (ms *Server) GetMessage(c context.Context, req *mp.GetMessageReq) (*mp.GetMessageResp, error) {
	if !ms.validator.ValidateJwt(req.AccToken) {
		return ms.factory.GetMessageResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if m := ms.messageCenter.GetMessage(fm.ConvertId(req.Id)); m != nil {
		return ms.factory.GetMessageResp(1, help.StatusCode_OK, m), nil
	}

	return ms.factory.GetMessageResp(0, help.StatusCode_OK, nil), nil
}

func (ms *Server) GetMessages(c context.Context, req *mp.GetMessagesReq) (*mp.GetMessagesResp, error) {
	if !ms.validator.ValidateJwt(req.AccToken) {
		return ms.factory.GetMessagesResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if messages := ms.messageCenter.GetMessages(fc.ConvertChatId(req.Id), int(req.Page)); ms != nil {
		return ms.factory.GetMessagesResp(1, help.StatusCode_OK, messages), nil
	}

	return ms.factory.GetMessagesResp(0, help.StatusCode_OK, nil), nil
}

func (ms *Server) GetLastMessages(c context.Context, req *mp.GetLastMessagesReq) (*mp.GetLastMessagesResp, error) {
	if !ms.validator.ValidateJwt(req.AccToken) {
		return ms.factory.GetLastMessagesResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if messages := ms.messageCenter.GetLastMessages(fc.ConvertChatIds(req.Ids)); messages != nil {
		return ms.factory.GetLastMessagesResp(1, help.StatusCode_OK, messages), nil
	}

	return ms.factory.GetLastMessagesResp(0, help.StatusCode_OK, nil), nil
}

func (ms *Server) GetUpdates(c context.Context, req *mp.GetUpdatesReq) (*mp.GetUpdatesResp, error) {
	if !ms.validator.ValidateJwt(req.AccToken) {
		return ms.factory.GetUpdatesResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if messages := ms.messageCenter.GetUpdates(fc.ConvertChatId(req.Id), req.Date); messages != nil {
		return ms.factory.GetUpdatesResp(1, help.StatusCode_OK, messages), nil
	}

	return ms.factory.GetUpdatesResp(0, help.StatusCode_OK, nil), nil
}

func (ms *Server) AddMessage(c context.Context, req *mp.AddMessageReq) (*mp.AddMessageResp, error) {
	id, res := ms.validator.ValidateJwtId(req.AccToken)
	if !res {
		return ms.factory.AddMessageResp(0, help.StatusCode_FORBIDDEN, nil, 0), nil
	}

	if mid := fm.NewFrom(req.Text, id, fc.ConvertChatId(req.ChatId), fm.ConvertIds(req.Messages)); ms.messageCenter.AddMessage(mid, req.DevToken) {
		return ms.factory.AddMessageResp(1, help.StatusCode_OK, &mid.Id, mid.Date), nil
	}

	return ms.factory.AddMessageResp(0, help.StatusCode_OK, nil, 0), nil
}

func (ms *Server) EditMessage(c context.Context, req *mp.EditMessageReq) (*mp.EditMessageResp, error) {
	id, res := ms.validator.ValidateJwtId(req.AccToken)
	if !res {
		return ms.factory.EditMessageResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if ms.messageCenter.EditMessage(id, fm.ConvertShortMessage(req.Message)) {
		return ms.factory.EditMessageResp(1, help.StatusCode_OK), nil
	}

	return ms.factory.EditMessageResp(0, help.StatusCode_OK), nil
}

func (ms *Server) DelMessage(c context.Context, req *mp.DelMessageReq) (*mp.DelMessageResp, error) {
	id, res := ms.validator.ValidateJwtId(req.AccToken)
	if !res {
		return ms.factory.DelMessageResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if ms.messageCenter.DelMessage(fm.ConvertId(req.Id), id) {
		return ms.factory.DelMessageResp(1, help.StatusCode_OK), nil
	}

	return ms.factory.DelMessageResp(0, help.StatusCode_OK), nil
}