package user

import (
	cv "centers/validate"
	"context"
	up "services/user/proto"
	cu "centers/user"
	fr "factories/response"
	"help"
	fs "factories/status"
	fu "factories/user"
	fp "factories/phone"
)

type Server struct {
	userCenter *cu.UserCenter
	validator *cv.ValidateCenter
	factory *fr.UserFactory
}

// static

func New(valdiator *cv.ValidateCenter) (*Server) {
	userCenter, err := cu.New()
	if err != nil {
		panic(err)
	}
	factory := &fr.UserFactory {}
	return &Server { userCenter, valdiator, factory }
}

// public rpc

func (us *Server) Get(c context.Context, req *up.GetReq) (*up.GetResp, error) {
	if !us.validator.ValidateJwt(req.AccToken) {
		return us.factory.GetResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if user := us.userCenter.Get(fu.ConvertUId(req.Id)); user != nil {
		return us.factory.GetResp(1, help.StatusCode_OK, user), nil
	}

	return us.factory.GetResp(0, help.StatusCode_OK, nil), nil
}

func (us *Server) GetByPhone(c context.Context, req *up.GetByPhoneReq) (*up.GetByPhoneResp, error) {
	if !us.validator.ValidateJwt(req.AccToken) {
		return us.factory.GetByPhoneResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if user := us.userCenter.GetByPhone(fp.ConvertPhoneU(req.Phone)); user != nil {
		return us.factory.GetByPhoneResp(1, help.StatusCode_OK, user), nil
	}

	return us.factory.GetByPhoneResp(0, help.StatusCode_OK, nil), nil
}

func (us *Server) GetByUsername(c context.Context, req *up.GetByUsernameReq) (*up.GetByUsernameResp, error) {
	if !us.validator.ValidateJwt(req.AccToken) {
		return us.factory.GetByUsernameResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if user := us.userCenter.GetByUsername(req.Username); user != nil {
		return us.factory.GetByUsernameResp(1, help.StatusCode_OK, user), nil
	}

	return us.factory.GetByUsernameResp(0, help.StatusCode_OK, nil), nil
}

func (us *Server) GetByPhones(c context.Context, req *up.GetByPhonesReq) (*up.GetByPhonesResp, error) {
	if !us.validator.ValidateJwt(req.AccToken) {
		return us.factory.GetByPhonesResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if users := us.userCenter.GetByPhones(fp.ConvertPhonesU(req.Phones)); users != nil {
		return us.factory.GetByPhonesResp(1, help.StatusCode_OK, users), nil
	}

	return us.factory.GetByPhonesResp(0, help.StatusCode_OK, nil), nil
}

func (us *Server) SearchByUsername(c context.Context, req *up.SearchByUsernameReq) (*up.SearchByUsernameResp, error) {
	if !us.validator.ValidateJwt(req.AccToken) {
		return us.factory.SearchByUsernameResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if users := us.userCenter.SearchByUsername(req.Username); users != nil {
		return us.factory.SearchByUsernameResp(1, help.StatusCode_OK, users), nil
	}

	return us.factory.SearchByUsernameResp(0, help.StatusCode_OK, nil), nil
}

func (us *Server) EditName(c context.Context, req *up.EditNameReq) (*up.EditNameResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.EditNameResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.EditName(req.Name, id) {
		return us.factory.EditNameResp(1, help.StatusCode_OK), nil
	}

	return us.factory.EditNameResp(0, help.StatusCode_OK), nil
}

func (us *Server) EditUsername(c context.Context, req *up.EditUsernameReq) (*up.EditUsernameResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.EditUsernameResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.EditUsername(req.Username, id) {
		return us.factory.EditUsernameResp(1, help.StatusCode_OK), nil
	}

	return us.factory.EditUsernameResp(0, help.StatusCode_OK), nil
}

func (us *Server) EditPhone(c context.Context, req *up.EditPhoneReq) (*up.EditPhoneResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.EditPhoneResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.EditPhone(fp.ConvertPhoneU(req.Phone), id) {
		return us.factory.EditPhoneResp(1, help.StatusCode_OK), nil
	}

	return us.factory.EditPhoneResp(0, help.StatusCode_OK), nil
}

func (us *Server) EditPassword(c context.Context, req *up.EditPasswordReq) (*up.EditPasswordResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.EditPasswordResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.EditPassword(req.OldPassword, req.NewPassword, id) {
		return us.factory.EditPasswordResp(1, help.StatusCode_OK), nil
	}

	return us.factory.EditPasswordResp(0, help.StatusCode_OK), nil
}

func (us *Server) SetStatus(c context.Context, req *up.SetStatusReq) (*up.SetStatusResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.SetStatusResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.SetStatus(fs.Convert(*req.Status), req.Token, id) {
		return us.factory.SetStatusResp(1, help.StatusCode_OK), nil
	}

	return us.factory.SetStatusResp(0, help.StatusCode_OK), nil
}

func (us *Server) AddToken(c context.Context, req *up.AddTokenReq) (*up.AddTokenResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.AddTokenResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.AddToken(req.Token, id) {
		return us.factory.AddTokenResp(1, help.StatusCode_OK), nil
	}

	return us.factory.AddTokenResp(0, help.StatusCode_OK), nil
}

func (us *Server) DelToken(c context.Context, req *up.DelTokenReq) (*up.DelTokenResp, error) {
	id, res := us.validator.ValidateJwtId(req.AccToken)
	if !res {
		return us.factory.DelTokenResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if us.userCenter.DelToken(req.Token, id) {
		return us.factory.DelTokenResp(1, help.StatusCode_OK), nil
	}

	return us.factory.DelTokenResp(0, help.StatusCode_OK), nil
}
