package chat

import (
	"context"
	cp "services/chat/proto"
	cc "centers/chat"
	cv "centers/validate"
	fc "factories/chat"
	"help"
	fu "factories/user"
	fr "factories/response"
)

type Server struct {
	chatCenter 	*cc.ChatCenter
	validator 	*cv.ValidateCenter
	factory 	*fr.ChatFactory
}

// static

func New(validator *cv.ValidateCenter) (*Server) {
	chatCenter, err := cc.New()
	if err != nil {
		panic(err)
	}
	factory := &fr.ChatFactory {}
	return &Server{ chatCenter, validator, factory }
}

// public rpc

func (cs *Server) GetChat(c context.Context, req *cp.GetChatReq) (*cp.GetChatResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.GetChatResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if ch := cs.chatCenter.GetChat(fc.ConvertId(req.Id), id); ch != nil {
		return cs.factory.GetChatResp(1, help.StatusCode_OK, ch), nil
	}

	return cs.factory.GetChatResp(0, help.StatusCode_OK, nil), nil
}

func (cs *Server) GetChatWith(c context.Context, req *cp.GetChatWithReq) (*cp.GetChatWithResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.GetChatWithResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if ch := cs.chatCenter.GetChatWith(fu.ConvertId(req.Id), id); ch != nil {
		return cs.factory.GetChatWithResp(1, help.StatusCode_OK, ch), nil
	}

	return cs.factory.GetChatWithResp(0, help.StatusCode_OK, nil), nil
}

func (cs *Server) GetChats(c context.Context, req *cp.GetChatsReq) (*cp.GetChatsResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.GetChatsResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if chs := cs.chatCenter.GetChats(id); chs != nil {
		return cs.factory.GetChatsResp(1, help.StatusCode_OK, chs), nil
	}

	return cs.factory.GetChatsResp(0, help.StatusCode_OK, nil), nil
}

func (cs *Server) AddDialog(c context.Context, req *cp.AddDialogReq) (*cp.AddDialogResp, error) {
	id, val := cs.validator.ValidateJwtId(req.AccToken)
	if !val {
		return cs.factory.AddDialogResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if ch := cs.chatCenter.AddDialog(id, fu.ConvertId(req.Id), req.DevToken); ch != nil {
		return cs.factory.AddDialogResp(1, help.StatusCode_OK, ch), nil
	}

	return cs.factory.AddDialogResp(0, help.StatusCode_OK, nil), nil
}

func (cs *Server) AddGroup(c context.Context, req *cp.AddGroupReq) (*cp.AddGroupResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.AddGroupResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if ch := cs.chatCenter.AddGroup(req.Title, id, fu.ConvertIds(req.Users)); ch != nil {
		return cs.factory.AddGroupResp(1, help.StatusCode_OK, ch), nil
	}

	return cs.factory.AddGroupResp(0, help.StatusCode_OK, nil), nil
}

func (cs *Server) AddChannel(c context.Context, req *cp.AddChannelReq) (*cp.AddChannelResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.AddChannelResp(0, help.StatusCode_FORBIDDEN, nil), nil
	}

	if ch := cs.chatCenter.AddChannel(req.Title, id); ch != nil {
		return cs.factory.AddChannelResp(1, help.StatusCode_OK, ch), nil
	}

	return cs.factory.AddChannelResp(0, help.StatusCode_OK, nil), nil
}

func (cs *Server) EditChat(c context.Context, req *cp.EditChatReq) (*cp.EditChatResp, error) {
	if !cs.validator.ValidateJwt(req.AccToken) {
		return cs.factory.EditChatResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if cs.chatCenter.EditChat(fc.ConvertId(req.Id), req.Title) {
		return cs.factory.EditChatResp(1, help.StatusCode_OK), nil
	}

	return cs.factory.EditChatResp(0, help.StatusCode_OK), nil
}

func (cs *Server) AddUser(c context.Context, req *cp.AddUserReq) (*cp.AddUserResp, error) {
	if !cs.validator.ValidateJwt(req.AccToken) {
		return cs.factory.AddUserResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if cs.chatCenter.AddUser(fc.ConvertId(req.Id), fu.ConvertId(req.UserId)) {
		return cs.factory.AddUserResp(1, help.StatusCode_OK), nil
	}

	return cs.factory.AddUserResp(0, help.StatusCode_OK), nil
}

func (cs *Server) DelUser(c context.Context, req *cp.DelUserReq) (*cp.DelUserResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.DelUserResp(0, help.StatusCode_FORBIDDEN), nil
	}

	if cs.chatCenter.DelUser(fc.ConvertId(req.Id), id, fu.ConvertId(req.UserId)) {
		return cs.factory.DelUserResp(1, help.StatusCode_OK), nil
	}

	return cs.factory.DelUserResp(0, help.StatusCode_OK), nil
}

func (cs *Server) UpdateLast(c context.Context, req *cp.UpdateLastReq) (*cp.UpdateLastResp, error) {
	id, res := cs.validator.ValidateJwtId(req.AccToken)
	if !res {
		return cs.factory.UpdateLastResp(0, help.StatusCode_FORBIDDEN, 0), nil
	}

	if res, date := cs.chatCenter.UpdateLast(fc.ConvertId(req.ChatId), id, req.DevToken); res {
		return cs.factory.UpdateLastResp(1, help.StatusCode_OK, date), nil
	}

	return cs.factory.UpdateLastResp(0, help.StatusCode_OK, 0), nil
}
