package factories_chat

import (
	cp "services/chat/proto"
	ec "entities/chat"
	fu "factories/user"
	eu "entities/user"
	"time"
	mp "services/message/proto"
)

func ConvertId(id *cp.Id) (*ec.Id) {
	return &ec.Id { Value: id.Value }
}

func ConvertChatId(id *mp.ChatId) (*ec.Id) {
	return &ec.Id { Value: id.Value }
}

func ConvertChatIds(ids []*mp.ChatId) ([]*ec.Id) {
	res := []*ec.Id{}
	for _, id := range ids {
		res = append(res, ConvertChatId(id))
	}
	return res
}

func ReverseConvertChat(ch *ec.Chat) (*cp.Chat) {
	if ch == nil {
		return &cp.Chat {}
	}
	return &cp.Chat {
		Id: ch.Id.Value,
		Title: ch.Title,
		Type: ch.Type,
		Created: ch.Created,
		Users: fu.ReverseConvertUserIds(ch.Users),
		Admins: fu.ReverseConvertUserIds(ch.Admins),
		LastUpdate: ch.LastUpdate(),
		LastUpdateFriend: ch.LastUpdateFriend(),
		BadgeCount: int32(ch.BadgeCount()),
	}
}

func ReverseConvertChats(chs []*ec.Chat) ([]*cp.Chat) {
	if chs == nil {
		return []*cp.Chat {}
	}
	res := []*cp.Chat{}
	for _, c := range chs {
		res = append(res, ReverseConvertChat(c))
	}
	return res
}

func NewDialog(f *eu.Id, s *eu.Id) (*ec.Chat) {
	ch := &ec.Chat {
		Id: ec.NewId(),
		Title: "",
		Type: ec.Dialog,
		Created: time.Now().Unix(),
		Users: []*eu.Id { f, s },
		Admins: []*eu.Id {},
	}
	ch.SetLastUpdate(nil)
	ch.SetLastUpdateFriend(nil)
	ch.SetBadgeCount(0)
	return ch
}

func NewGroup(title string, adminId *eu.Id, ids []*eu.Id) (*ec.Chat) {
	ch := &ec.Chat {
		Id: ec.NewId(),
		Title: title,
		Type: ec.Group,
		Created: time.Now().Unix(),
		Users: ids,
		Admins: []*eu.Id { adminId },
	}
	ch.SetLastUpdate(nil)
	ch.SetLastUpdateFriend(nil)
	ch.SetBadgeCount(0)
	return ch
}

func NewChannel(title string, adminId *eu.Id) (*ec.Chat) {
	ch := &ec.Chat {
		Id: ec.NewId(),
		Title: title,
		Type: ec.Channel,
		Created: time.Now().Unix(),
		Users: []*eu.Id {},
		Admins: []*eu.Id { adminId },
	}
	ch.SetLastUpdate(nil)
	ch.SetLastUpdateFriend(nil)
	ch.SetBadgeCount(0)
	return ch
}

func NewLast(chatId *ec.Id, userId *eu.Id, date int64) (*ec.LastUpdate) {
	return &ec.LastUpdate {
		ChatId: chatId,
		UserId: userId,
		Date: date,
		BadgeCount: 0,
	}
}