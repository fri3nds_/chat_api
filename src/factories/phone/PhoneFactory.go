package factories_phone

import (
	p "services/auth/proto"
	e "entities"
	up "services/user/proto"
)

// Convert part

func ConvertPhone(phone *p.Phone) (*e.Phone) {
	return &e.Phone { Number: phone.Number }
}

func ConvertPhoneU(phone *up.Phone) (*e.Phone) {
	return &e.Phone { Number: phone.Number }
}

func ConvertPhonesU(phones []*up.Phone) ([]*e.Phone) {
	res := []*e.Phone{}
	for _, phone := range phones {
		res = append(res, ConvertPhoneU(phone))
	}
	return res
}

// Reverse convert part

func ReverseConvertPhone(phone *e.Phone) (*p.Phone) {
	return &p.Phone { Number: phone.Number }
}

func ReverseConvertPhoneU(phone *e.Phone) (*up.Phone) {
	return &up.Phone { Number: phone.Number }
}