package factories_code

import (
	e "entities"
	"help"
	"time"
)

func New(phone e.Phone) (*e.Code) {
	tenMin := 10 * 60
	code := &e.Code {
		Phone: phone.Number,
		Value: help.GenerateRandomCode(),
		Exp: time.Now().Unix() + int64(tenMin),
	}
	return code
}
