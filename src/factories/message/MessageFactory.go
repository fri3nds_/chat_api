package factories_message

import (
	ec "entities/chat"
	em "entities/message"
	eu "entities/user"
	mp "services/message/proto"
	"time"
)

// New Part

func NewFrom(text string, userId *eu.Id, chatId *ec.Id, messages []*em.Id) (*em.ShortMes) {
	return &em.ShortMes {
		Id:       em.NewId(),
		Text:     text,
		UserId:   userId,
		ChatId:   chatId,
		Date:     time.Now().UnixNano(),
		Messages: messages,
	}
}

// Convert Messages

func MessageFromShort(sm *em.ShortMes) (*em.Message) {
	if sm == nil {
		return &em.Message { }
	}
	return &em.Message {
		Id: sm.Id,
		Text: sm.Text,
		UserId: sm.UserId,
		ChatId: sm.ChatId,
		Date: sm.Date,
		//Content []*Content
		Messages: []*em.Message {},
	}
}

func ShortFromMessage(m *em.Message) (*em.ShortMes) {
	if m == nil {
		return &em.ShortMes {}
	}
	return &em.ShortMes {
		Id:       m.Id,
		Text:     m.Text,
		UserId:   m.UserId,
		ChatId:   m.ChatId,
		Date:     m.Date,
		Messages: ConvertToIds(m.Messages),
	}
}

func ConvertShortMessage(m *mp.ShortMessage) (*em.ShortMes) {
	if m == nil {
		return &em.ShortMes {}
	}
	return &em.ShortMes {
		Id:       em.Id { Value: m.Id },
		Text:     m.Text,
		UserId:   &eu.Id { Value: m.UserId },
		ChatId:   &ec.Id { Value: m.ChatId },
		Date:     m.Date,
		Messages: ConvertIds(m.Messages),
	}
}

// Reverse Convert Messages

func ReverseConvertMessage(m *em.Message) (*mp.Message) {
	if m == nil {
		return &mp.Message {}
	}
	messages := []*mp.Message{}
	if len(m.Messages) > 0 {
		for _, m := range m.Messages {
			mes := ReverseConvertMessage(m)
			messages = append(messages, mes)
		}
	}
	return &mp.Message {
		Id: m.Id.Value,
		Text: m.Text,
		UserId: m.UserId.Value,
		ChatId: m.ChatId.Value,
		Date: m.Date,
		Messages: messages,
	}
}

func ReverseConvertMessages(ms []*em.Message) ([]*mp.Message) {
	if ms == nil {
		return []*mp.Message{}
	}
	res := []*mp.Message{}
	for _, m := range ms {
		res = append(res, ReverseConvertMessage(m))
	}
	return res
}

// Convert Ids

func ConvertId(id *mp.Id) (*em.Id) {
	if id == nil {
		return &em.Id{}
	}
	return &em.Id { Value: id.Value }
}

func ConvertIds(ids []*mp.Id) ([]*em.Id) {
	res := []*em.Id{}
	for _, id := range ids {
		res = append(res, ConvertId(id))
	}
	return res
}

func ConvertToIds(ms []*em.Message) ([]*em.Id) {
	if ms == nil {
		return []*em.Id{}
	}
	ids := []*em.Id{}
	for _, m := range ms {
		ids = append(ids, &m.Id)
	}
	return ids
}


