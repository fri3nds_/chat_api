package factories_user

import (
	e "entities"
	eu "entities/user"
	fp "factories/phone"
	fs "factories/status"
	"help"
	ap "services/auth/proto"
	cp "services/chat/proto"
	up "services/user/proto"
)

// Helps

func ArrFromIds(ids []*eu.Id) ([]string) {
	arr := []string{}
	for _, id := range ids {
		arr = append(arr, id.Value)
	}
	return arr
}

// New Part

func UserWithPhone(phone e.Phone) (*eu.User) {
	statuses := make([]e.Status, 0)
	statuses = append(statuses, *fs.New(e.Offline))
	return &eu.User {
		Id: eu.NewId(),
		Name: "",
		Username: "",
		Password: "",
		Phone: phone,
		Statuses: statuses,
		IOSTokens: make(map[string]int),
		Secret: help.GenerateRandomString(),
	}
}

func UserWithUsernameAndPassword(username, password string) (*eu.User) {
	statuses := make([]e.Status, 0)
	statuses = append(statuses, *fs.New(e.Offline))
	return &eu.User {
		Id: eu.NewId(),
		Name: "",
		Username: username,
		Password: password,
		Phone: e.Phone { Number: "" },
		Statuses: statuses,
		IOSTokens: make(map[string]int),
		Secret: help.GenerateRandomString(),
	}
}

// Reverse Convert User

func ReverseConvertUser(us *eu.User, accToken, refToken string) (*ap.User) {
	if us == nil {
		return &ap.User {}
	}
	return &ap.User {
		Id:       us.Id.Value,
		Name:     us.Name,
		Username: us.Username,
		Phone:    fp.ReverseConvertPhone(&us.Phone),
		Status:   fs.ReverseConvert(us.GetStatus()),
		AccToken: accToken,
		RefToken: refToken,
	}
}

func ReverseConvertUserU(us *eu.User) (*up.User) {
	if us == nil {
		return &up.User{}
	}
	return &up.User {
		Id: us.Id.Value,
		Name: us.Name,
		Username: us.Username,
		Phone: fp.ReverseConvertPhoneU(&us.Phone),
		Status: fs.ReverseConvertU(us.GetStatus()),
		BadgeCount: us.BadgeCount,
	}
}

func ReverseConvertUsersU(users []*eu.User) ([]*up.User) {
	if users == nil {
		return []*up.User{}
	}
	res := []*up.User{}
	for _, user := range users {
		res = append(res, ReverseConvertUserU(user))
	}
	return res
}

// Convert Ids

func ConvertId(id *cp.UserId) (*eu.Id) {
	return &eu.Id { Value: id.Value }
}

func ConvertIds(ids []*cp.UserId) ([]*eu.Id) {
	if ids == nil {
		return []*eu.Id {}
	}
	res := []*eu.Id {}
	for i, id := range ids {
		res[i] = ConvertId(id)
	}
	return res
}

func ConvertUId(id *up.Id) (*eu.Id) {
	return &eu.Id { Value: id.Value }
}

// Reverse Convert Ids

func ReverseConvertUserIds(ids []*eu.Id) ([]*cp.UserId) {
	if ids == nil {
		return []*cp.UserId {}
	}
	res := []*cp.UserId {}
	for _, id := range ids {
		res = append(res, &cp.UserId { Value: id.Value })
	}
	return res
}

