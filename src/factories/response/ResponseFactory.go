package factories_response

import (
	ec "entities/chat"
	cp "services/chat/proto"
	fc "factories/chat"
	eu "entities/user"
	ap "services/auth/proto"
	fu "factories/user"
	fm "factories/message"
	em "entities/message"
	mp "services/message/proto"
	up "services/user/proto"
)

// Auth Part

type AuthFactory struct {}

func (af *AuthFactory) VerifyResp(res int, user *eu.User, accToken, refToken string) (*ap.VerifyResp) {
	return &ap.VerifyResp {
		Result: &ap.Result { Value: int32(res) },
		User: fu.ReverseConvertUser(user, accToken, refToken),
	}
}

func (af *AuthFactory) LoginWithoutPhone(res int, user *eu.User, accToken, refToken string) (*ap.LoginWithoutPhoneResp) {
	return &ap.LoginWithoutPhoneResp {
		Result: &ap.Result { Value: int32(res) },
		User: fu.ReverseConvertUser(user, accToken, refToken),
	}
}

func (af *AuthFactory) Register(res int) (*ap.RegisterResp) {
	return &ap.RegisterResp { Result: &ap.Result { Value: int32(res) } }
}

func (af *AuthFactory) LogoutResp(res, code int) (*ap.LogoutResp) {
	return &ap.LogoutResp {
		Result: &ap.Result { Value: int32(res) },
		Code: &ap.StatusCode { Value: int32(code) },
	}
}

func (af *AuthFactory) RefResp(res, code int, accToken, refToken string) (*ap.RefResp) {
	return &ap.RefResp {
		Result: &ap.Result { Value: int32(res) },
		Code: &ap.StatusCode { Value: int32(code) },
		AccToken: accToken,
		RefToken: refToken,
	}
}

// Chat Part

type ChatFactory struct {}

func (cf *ChatFactory) GetChatResp(res, code int, ch *ec.Chat) (*cp.GetChatResp) {
	return &cp.GetChatResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Chat: fc.ReverseConvertChat(ch),
	}
}

func (cf *ChatFactory) GetChatWithResp(res, code int, ch *ec.Chat) (*cp.GetChatWithResp) {
	return &cp.GetChatWithResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Chat: fc.ReverseConvertChat(ch),
	}
}

func (cf *ChatFactory) GetChatsResp(res, code int, chs []*ec.Chat) (*cp.GetChatsResp) {
	return &cp.GetChatsResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Chats: fc.ReverseConvertChats(chs),
	}
}

func (cf *ChatFactory) AddDialogResp(res, code int, ch *ec.Chat) (*cp.AddDialogResp) {
	return &cp.AddDialogResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Chat: fc.ReverseConvertChat(ch),
	}
}

func (cf *ChatFactory) AddGroupResp(res, code int, ch *ec.Chat) (*cp.AddGroupResp) {
	return &cp.AddGroupResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Chat: fc.ReverseConvertChat(ch),
	}
}

func (cf *ChatFactory) AddChannelResp(res, code int, ch *ec.Chat) (*cp.AddChannelResp) {
	return &cp.AddChannelResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Chat: fc.ReverseConvertChat(ch),
	}
}

func (cf *ChatFactory) EditChatResp(res, code int) (*cp.EditChatResp) {
	return &cp.EditChatResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
	}
}

func (cf *ChatFactory) AddUserResp(res, code int) (*cp.AddUserResp) {
	return &cp.AddUserResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
	}
}

func (cf *ChatFactory) DelUserResp(res, code int) (*cp.DelUserResp) {
	return &cp.DelUserResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
	}
}

func (cf *ChatFactory) UpdateLastResp(res, code int, date int64) (*cp.UpdateLastResp) {
	return &cp.UpdateLastResp {
		Result: &cp.Result { Value: int32(res) },
		Code: &cp.StatusCode { Value: int32(code) },
		Date: date,
	}
}

// Message Part

type MessageFactory struct {}

func (mf *MessageFactory) GetMessageResp(res, code int, m *em.Message) (*mp.GetMessageResp) {
	return &mp.GetMessageResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
		Message: fm.ReverseConvertMessage(m),
	}
}

func (mf *MessageFactory) GetMessagesResp(res, code int, ms []*em.Message) (*mp.GetMessagesResp) {
	return &mp.GetMessagesResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
		Messages: fm.ReverseConvertMessages(ms),
	}
}

func (mf *MessageFactory) GetLastMessagesResp(res, code int, ms []*em.Message) (*mp.GetLastMessagesResp) {
	return &mp.GetLastMessagesResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
		Messages: fm.ReverseConvertMessages(ms),
	}
}

func (mf *MessageFactory) GetUpdatesResp(res, code int, ms []*em.Message) (*mp.GetUpdatesResp) {
	return &mp.GetUpdatesResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
		Messages: fm.ReverseConvertMessages(ms),
	}
}

func (mf *MessageFactory) AddMessageResp(res, code int, id *em.Id, date int64) (*mp.AddMessageResp) {
	return &mp.AddMessageResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
		Id: &mp.Id { Value: id.Value },
		Date: date,
	}
}

func (mf *MessageFactory) EditMessageResp(res, code int) (*mp.EditMessageResp) {
	return &mp.EditMessageResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
	}
}

func (mf *MessageFactory) DelMessageResp(res, code int) (*mp.DelMessageResp) {
	return &mp.DelMessageResp {
		Result: &mp.Result { Value: int32(res) },
		Code: &mp.StatusCode { Value: int32(code) },
	}
}

// User Part

type UserFactory struct {}

func (uf *UserFactory) GetResp(res, code int, user *eu.User) (*up.GetResp) {
	return &up.GetResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
		User: fu.ReverseConvertUserU(user),
	}
}

func (uf *UserFactory) GetByPhoneResp(res, code int, user *eu.User) (*up.GetByPhoneResp) {
	return &up.GetByPhoneResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
		User: fu.ReverseConvertUserU(user),
	}
}

func (uf *UserFactory) GetByUsernameResp(res, code int, user *eu.User) (*up.GetByUsernameResp) {
	return &up.GetByUsernameResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
		User: fu.ReverseConvertUserU(user),
	}
}

func (uf *UserFactory) GetByPhonesResp(res, code int, users []*eu.User) (*up.GetByPhonesResp) {
	return &up.GetByPhonesResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
		Users: fu.ReverseConvertUsersU(users),
	}
}

func (uf *UserFactory) SearchByUsernameResp(res, code int, users []*eu.User) (*up.SearchByUsernameResp) {
	return &up.SearchByUsernameResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
		Users: fu.ReverseConvertUsersU(users),
	}
}

func (uf *UserFactory) EditNameResp(res, code int) (*up.EditNameResp) {
	return &up.EditNameResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}

func (uf *UserFactory) EditUsernameResp(res, code int) (*up.EditUsernameResp) {
	return &up.EditUsernameResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}

func (uf *UserFactory) EditPhoneResp(res, code int) (*up.EditPhoneResp) {
	return &up.EditPhoneResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}

func (uf *UserFactory) EditPasswordResp(res, code int) (*up.EditPasswordResp) {
	return &up.EditPasswordResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}

func (uf *UserFactory) SetStatusResp(res, code int) (*up.SetStatusResp) {
	return &up.SetStatusResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}

func (uf *UserFactory) AddTokenResp(res, code int) (*up.AddTokenResp) {
	return &up.AddTokenResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}

func (uf *UserFactory) DelTokenResp(res, code int) (*up.DelTokenResp) {
	return &up.DelTokenResp {
		Result: &up.Result { Value: int32(res) },
		Code: &up.StatusCode { Value: int32(code) },
	}
}