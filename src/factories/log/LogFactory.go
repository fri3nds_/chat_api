package factories_log

import (
	el "entities/log"
	"time"
)

func New(title, description string) (*el.Log) {
	return &el.Log {
		Id: el.NewId(),
		Date: time.Now().Unix(),
		Title: title,
		Description: description,
	}
}
