package factories_status

import (
	e "entities"
	"time"
	up "services/user/proto"
	ap "services/auth/proto"
)

func New(value int) (*e.Status) {
	return &e.Status {
		Date: time.Now().Unix(),
		Value: value,
	}
}

// Convert Part

func Convert(status up.Status) (e.Status) {
	return e.Status {
		Value: int(status.Value),
		Date: time.Now().Unix(),
	}
}

// Reverse Convert Part

func ReverseConvert(status e.Status) (*ap.Status) {
	return &ap.Status { Value: int32(status.Value), Date: int64(status.Date) }
}

func ReverseConvertU(status e.Status) (*up.Status) {
	return &up.Status { Value: int32(status.Value), Date: int64(status.Date) }
}