package main

import (
	"log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	pb "services/ping"
	ap "services/auth/proto"
	sa "services/auth"
	pp "services/ping/proto"
	cl "centers/log"
	cv "centers/validate"
	scp "services/chat/proto"
	sc "services/chat"
	mp "services/message/proto"
	sm "services/message"
	up "services/user/proto"
	su "services/user"
	cp "centers/push"
	co "centers/observe"
	"os"
	"configs"
	"fmt"
)

func registerObservers() {
	cp.Default().AddObserver(co.DefaultTokenObserver().TokenInvalid, co.TokenInvalid, nil)
	cp.Default().AddObserver(co.Default().DialogDidAdded, co.DialogDidAdded, nil)
	cp.Default().AddObserver(co.Default().MessageDidAdded, co.MessageDidAdded, nil)
	cp.Default().AddObserver(co.Default().LastDidUpdate, co.LastDidUpdate, nil)
	cp.Default().AddObserver(co.Default().LastDidUpdateFriend, co.LastDidUpdateFriend, nil)
}

func removeObservers() {
	cp.Default().RemoveObserver(co.TokenInvalid, nil)
	cp.Default().RemoveObserver(co.DialogDidAdded, nil)
	cp.Default().RemoveObserver(co.MessageDidAdded, nil)
	cp.Default().RemoveObserver(co.LastDidUpdate, nil)
	cp.Default().RemoveObserver(co.LastDidUpdateFriend, nil)
}

func setMode(args []string) {
	if len(args) > 1 && args[1] == configs.ProductionMode {
		configs.Init(configs.ProductionMode)
	} else {
		configs.Init(configs.DevelopmentMode)
	}
}

func main() {
	setMode(os.Args)

	fmt.Println(configs.Default().Port(), configs.Default().Mode())
	lis, err := net.Listen("tcp", configs.Default().Port())
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	// Register push and observer centers
	cp.Default()
	co.Default()

	// Register notifications
	registerObservers()
	defer removeObservers()

	// Register validator
	validator := cv.New()

	// Register servers
	pp.RegisterPingerServer(s, &pb.Server{})
	ap.RegisterAuthServer(s, sa.New(validator))
	scp.RegisterChServer(s, sc.New(validator))
	mp.RegisterMesServer(s, sm.New(validator))
	up.RegisterUsServer(s, su.New(validator))

	// Register logs
	cl.Default.Init()

	// Register reflection service on gRPC pinger.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
