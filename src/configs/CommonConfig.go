package configs

const (
	ProductionMode 	= "production"
	DevelopmentMode = "development"

	ProductionPort 	= ":1488"
	DevelopmentPort = ":228"
)

type Config struct {
	mode string
}

// static part

var conf *Config

func Init(mode string) {
	conf = &Config { mode: mode }
}

func Default() (*Config) {
	return conf
}

// public part

func (c *Config) Mode() (string) {
	return c.mode
}

func (c *Config) Port() (string) {
	if c.mode == ProductionMode {
		return ProductionPort
	} else {
		return DevelopmentPort
	}
}

func (c *Config) IsProduction() (bool) {
	return c.mode == ProductionMode
}