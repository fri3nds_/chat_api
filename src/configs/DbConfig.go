package configs

const (
	DBAddress   		= "localhost:27017"
	Database    		= "chat"
	DatabaseDev 		= "chat_dev"

	UserCollection 		= "user"
	CodeCollection 		= "code"
	ChatCollection 		= "chat"
	LastCollection		= "last"
	MessageCollection 	= "message"
	LogCollection 		= "log"
)

// public part

func (c *Config) Database() (string) {
	if c.mode == ProductionMode {
		return Database
	} else {
		return DatabaseDev
	}
}