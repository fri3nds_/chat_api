package entities_message

import (
	ec "entities/chat"
	eu "entities/user"
)

type ShortMes struct {
	Id Id
	Text string
	UserId *eu.Id
	ChatId *ec.Id
	Date int64
	//Content []*Content
	Messages []*Id
}

type Message struct {
	Id Id
	Text string
	UserId *eu.Id
	ChatId *ec.Id
	Date int64
	//Content []*Content
	Messages []*Message
}

// public

func (m *Message) Map() (map[string]interface{}) {
	mapp := make(map[string]interface{})
	mapp["id"] = m.Id.Value
	mapp["text"] = m.Text
	mapp["userId"] = m.UserId.Value
	mapp["chatId"] = m.ChatId.Value
	mapp["date"] = m.Date
	mapp["messages"] = m.arrFromMessages()
	return mapp
}

func (sm *ShortMes) Map() (map[string]interface{}) {
	mapp := make(map[string]interface{})
	mapp["id"] = sm.Id.Value
	mapp["text"] = sm.Text
	mapp["userId"] = sm.UserId.Value
	mapp["chatId"] = sm.ChatId.Value
	mapp["date"] = sm.Date
	mapp["messages"] = sm.arrFromIds()
	return mapp
}

// private

func (m *Message) arrFromMessages() ([]string) {
	arr := []string{}
	for _, m := range m.Messages {
		arr = append(arr, m.Id.Value)
	}
	return arr
}

func (m *ShortMes) arrFromIds() ([]string) {
	arr := []string{}
	for _, id := range m.Messages {
		arr = append(arr, id.Value)
	}
	return arr
}
