package entities

const Offline 	= 0
const Online 	= 1

type Status struct {
	Date 	int64
	Value 	int
}