package entities

import "time"

type Code struct {
	Phone string
	Value string
	Exp int64
}

func (c *Code) Valid() (bool) {
	return time.Now().Unix() < c.Exp
}