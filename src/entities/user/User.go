package entities_user

import (
	e "entities"
	fs "factories/status"
)

// public

type User struct {
	Id 			Id
	Name 		string
	Username	string
	Password 	string
	Phone 		e.Phone
	Statuses 	[]e.Status
	IOSTokens	map[string]int
	Secret 		string
	BadgeCount	int32
}

func (us *User) GetStatus() (e.Status) {
	return us.Statuses[len(us.Statuses) - 1]
}

func (us *User) AddStatus(status e.Status, token string) {
	if us.contains(token) && us.IOSTokens[token] != status.Value {
		us.IOSTokens[token] = status.Value
	}
	us.updateStatus()
}

func (us *User) AddToken(token string) {
	us.IOSTokens[token] = 1
	us.updateStatus()
}

func (us *User) DelToken(token string) {
	if us.contains(token) {
		delete(us.IOSTokens, token)
	}
	us.updateStatus()
}

func (us *User) AddBadge(count int32) {
	us.BadgeCount += count
}

func (us *User) SubBadge(count int32) {
	if us.BadgeCount - count < 0 {
		us.BadgeCount = 0
	} else {
		us.BadgeCount -= count
	}
}

// private

func (u *User) contains(token string) (bool) {
	for k := range u.IOSTokens {
		if k == token {
			return true
		}
	}
	return false
}

func (u *User) clipStatuses() {
	for len(u.Statuses) > 3 {
		u.Statuses = u.Statuses[1:]
	}
}

func (u *User) tokenStatus() (int) {
	st := 0
	for _, s := range u.IOSTokens {
		st |= s
	}
	return st
}

func (u *User) updateStatus() {
	if len(u.Statuses) > 0 && u.tokenStatus() != u.GetStatus().Value {
		u.Statuses = append(u.Statuses, *fs.New(u.tokenStatus()))
		u.clipStatuses()
	}
}