package entities_chat

import (
	eu "entities/user"
	fu "factories/user"
)

const Dialog 	= 1
const Group		= 2
const Channel 	= 3

type Chat struct {
	Id 					Id
	Title 				string
	Type 				int32
	Created				int64
	Users 				[]*eu.Id
	Admins 				[]*eu.Id
	lastUpdate 			int64
	lastUpdateFriend	int64
	badgeCount  		int32
}

// public

func (ch *Chat) Map() (map[string]interface{}) {
	mapp := make(map[string]interface{})
	mapp["id"] = ch.Id.Value
	mapp["title"] = ch.Title
	mapp["type"] = ch.Type
	mapp["created"] = ch.Created
	mapp["users"] = fu.ArrFromIds(ch.Users)
	mapp["admins"] = fu.ArrFromIds(ch.Admins)
	mapp["lastUpdate"] = ch.lastUpdate
	mapp["lastUpdateFriend"] = ch.lastUpdateFriend
	mapp["badgeCount"] = ch.badgeCount
	return mapp
}

func (ch *Chat) LastUpdate() (int64) {
	return ch.lastUpdate
}

func (ch *Chat) SetLastUpdate(last *LastUpdate) {
	if last != nil {
		ch.lastUpdate = last.Date
		ch.badgeCount = last.BadgeCount
	} else {
		ch.lastUpdate = 0
		ch.badgeCount = 0
	}
}

func (ch *Chat) LastUpdateFriend() (int64) {
	return ch.lastUpdateFriend
}

func (ch *Chat) SetLastUpdateFriend(last *LastUpdate) {
	if last != nil {
		ch.lastUpdateFriend = last.Date
	} else {
		ch.lastUpdateFriend = 0
	}
}

func (ch *Chat) BadgeCount() (int32) {
	return ch.badgeCount
}

func (ch *Chat) SetBadgeCount(count int32) {
	if count > 0 {
		ch.badgeCount = count
	} else {
		ch.badgeCount = 0
	}
}