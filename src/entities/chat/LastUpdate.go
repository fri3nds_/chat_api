package entities_chat

import eu "entities/user"

type LastUpdate struct {
	ChatId 		*Id
	UserId 		*eu.Id
	Date 		int64
	BadgeCount 	int32
}

// public

func (last *LastUpdate) Map() (map[string]interface{}) {
	m := make(map[string]interface{})
	m["chatId"] = last.ChatId.Value
	m["date"] = last.Date
	m["badgeCount"] = last.BadgeCount
	return m
}