package entities_log

// public

type Log struct {
	Id 			Id
	Date 		int64
	Title		string
	Description string
}
