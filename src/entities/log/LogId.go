package entities_log

import "github.com/satori/go.uuid"

type Id struct {
	Value string
}

func NewId() (Id) {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}
	return Id { Value: u.String() }
}
