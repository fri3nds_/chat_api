package repositories_chat

import (
	"configs"
	ec "entities/chat"
	eu "entities/user"
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type LastUpdateRepo struct {
	session *mgo.Session
}

// static

func NewLast() (*LastUpdateRepo, error) {
	repo := &LastUpdateRepo{}
	session, err := mgo.Dial(configs.DBAddress)
	if err == nil {
		repo.session = session
	}
	return repo, err
}

// public

func (lr *LastUpdateRepo) Get(chatId *ec.Id, userId *eu.Id) (*ec.LastUpdate, error) {
	last := &ec.LastUpdate{}

	if err := lr.getCollection().Find(bson.M{ "chatid.value": chatId.Value, "userid.value": userId.Value }).One(&last); err != nil {
		return nil, errors.New("last not found")
	}

	return last, nil
}

func (lr *LastUpdateRepo) GetLastsByUser(id *eu.Id) ([]*ec.LastUpdate, error) {
	var lasts []*ec.LastUpdate

	if err := lr.getCollection().Find(bson.M{ "userid.value": id.Value }).All(&lasts); err != nil {
		return nil, errors.New("lasts not found")
	}

	return lasts, nil
}

func (lr *LastUpdateRepo) GetLastsByChat(id *ec.Id) ([]*ec.LastUpdate, error) {
	var lasts []*ec.LastUpdate

	if err := lr.getCollection().Find(bson.M{ "chatid.value": id.Value }).All(&lasts); err != nil {
		return nil, errors.New("lasts not found")
	}

	return lasts, nil
}

func (lr *LastUpdateRepo) GetLastFriend(chatId *ec.Id, currentUserId *eu.Id) (*ec.LastUpdate, error) {
	last := &ec.LastUpdate{}

	if err := lr.getCollection().Find(bson.M{ "chatid.value": chatId.Value, "userid.value": bson.M{ "$ne": currentUserId.Value } }).Sort("-date").One(&last); err != nil {
		return nil, errors.New("last not found")
	}

	return last, nil
}

func (lr *LastUpdateRepo) Add(last *ec.LastUpdate) (bool, error) {
	if lr.IsExist(last.ChatId, last.UserId) {
		return false, errors.New("last already exist")
	}

	if err := lr.getCollection().Insert(&last); err != nil {
		return false, err
	}

	return true, nil
}

func (lr *LastUpdateRepo) Save(last *ec.LastUpdate) (bool, error) {
	if !lr.IsExist(last.ChatId, last.UserId) {
		return false, errors.New("last not found")
	}

	if err := lr.getCollection().Update(bson.M{ "chatid.value": last.ChatId.Value, "userid.value": last.UserId.Value }, last); err != nil {
		return false, errors.New("last not found")
	}

	return true, nil
}

func (lr *LastUpdateRepo) Remove(chatId *ec.Id, userId *eu.Id) (bool, error) {
	if !lr.IsExist(chatId, userId) {
		return false, errors.New("last not found")
	}

	if err := lr.getCollection().Remove(bson.M{ "chatid.value": chatId.Value, "userid.value": userId.Value }); err != nil {
		return false, errors.New("last not found")
	}

	return true, nil
}

func (lr *LastUpdateRepo) IsExist(chatId *ec.Id, userId *eu.Id) (bool) {
	_, err := lr.Get(chatId, userId)
	return err == nil
}

func (lr *LastUpdateRepo) Clean() {
	lr.session.Close()
}

// private

func (lr *LastUpdateRepo) getCollection() (*mgo.Collection) {
	return lr.session.DB(configs.Default().Database()).C(configs.LastCollection)
}