package repositories_chat

import (
	"errors"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"
	"configs"
	ec "entities/chat"
	eu "entities/user"
)

type ChatRepo struct {
	session *mgo.Session
}

// static

func New() (*ChatRepo, error) {
	repo := &ChatRepo{}
	session, err := mgo.Dial(configs.DBAddress)
	if err == nil {
		repo.session = session
	}
	return repo, err
}

// public

func (cr *ChatRepo) Get(id ec.Id) (*ec.Chat, error) {
	ch := &ec.Chat{}

	if err := cr.getCollection().Find(bson.M{ "id.value": id.Value }).One(&ch); err != nil {
		return nil, errors.New("chat not found")
	}

	return ch, nil
}

func (cr *ChatRepo) GetWith(friendId eu.Id, id eu.Id) (*ec.Chat, error) {
	ch := &ec.Chat{}

	var condition [2]string
	condition[0] = friendId.Value
	condition[1] = id.Value

	first := bson.M{ "users": []bson.M{ { "value": friendId.Value }, { "value": id.Value } } }
	second := bson.M{ "users": []bson.M{ { "value": id.Value }, { "value": friendId.Value } } }

	if err := cr.getCollection().Find(bson.M{ "$or": []bson.M{ first, second } }).One(&ch); err != nil {
		return nil, errors.New("chat not found")
	}

	return ch, nil
}

func (cr *ChatRepo) GetByUser(id eu.Id) ([]*ec.Chat, error) {
	var chs []*ec.Chat

	if err := cr.getCollection().Find(bson.M{ "users": bson.M{ "$elemMatch": bson.M{ "value": id.Value } } }).All(&chs); err != nil {
		return nil, errors.New("chats not found")
	}

	return chs, nil
}

func (cr *ChatRepo) Add(chat *ec.Chat) (bool, error) {
	if chat.Type == ec.Dialog && cr.IsExistDialog(*chat.Users[0], *chat.Users[1]) {
		return false, errors.New("chat already exist")
	}

	if cr.IsExist(chat.Id) {
		return false, errors.New("chat already exist")
	}

	if err := cr.getCollection().Insert(&chat); err != nil {
		return false, err
	}

	return true, nil
}

func (cr *ChatRepo) Save(chat *ec.Chat) (bool, error) {
	if !cr.IsExist(chat.Id) {
		return false, errors.New("chat not found")
	}

	if err := cr.getCollection().Update(bson.M{ "id.value": chat.Id.Value }, chat); err != nil {
		return false, errors.New("chat not found")
	}

	return true, nil
}

func (cr *ChatRepo) Remove(id ec.Id) (bool, error) {
	if !cr.IsExist(id) {
		return false, errors.New("chat not found")
	}

	if err := cr.getCollection().Remove(bson.M{ "id.value": id.Value }); err != nil {
		return false, errors.New("chat not found")
	}

	return true, nil
}

func (cr *ChatRepo) IsExist(id ec.Id) (bool) {
	_, err := cr.Get(id)
	return err == nil
}

func (cr *ChatRepo) IsExistDialog(friendId eu.Id, id eu.Id) (bool) {
	_, err := cr.GetWith(friendId, id)
	return err == nil
}

func (cr *ChatRepo) Clean() {
	cr.session.Close()
}

// private

func (cr *ChatRepo) getCollection() (*mgo.Collection) {
	return cr.session.DB(configs.Default().Database()).C(configs.ChatCollection)
}