package repositories_message

import (
	"gopkg.in/mgo.v2"
	"configs"
	"gopkg.in/mgo.v2/bson"
	"errors"
	em "entities/message"
	ec "entities/chat"
	fm "factories/message"
)

type MessageRepo struct {
	session *mgo.Session
}

// static

func New() (*MessageRepo, error) {
	repo := &MessageRepo{}
	session, err := mgo.Dial(configs.DBAddress)
	if err == nil {
		repo.session = session
	}
	return repo, err
}

// public

func (mr *MessageRepo) Get(id em.Id) (*em.Message, error) {
	sm := &em.ShortMes{}

	if err := mr.getCollection().Find(bson.M{ "id.value": id.Value }).One(&sm); err != nil {
		return nil, errors.New("message not found")
	}

	m := fm.MessageFromShort(sm)
	m = mr.fillMessages(m, sm.Messages)

	return m, nil
}

func (mr *MessageRepo) GetByChat(id ec.Id, page, limit int) ([]*em.Message, error) {
	var mess []*em.ShortMes

	if err := mr.getCollection().Find(bson.M{ "chatid.value": id.Value }).Sort("-date").Skip(page * limit).Limit(limit).All(&mess); err != nil {
		return nil, errors.New("messages not found")
	}

	var messages []*em.Message
	for _, mes := range mess {
		m := fm.MessageFromShort(mes)
		m = mr.fillMessages(m, mes.Messages)
		messages = append(messages, m)
	}

	return messages, nil
}

func (mr *MessageRepo) GetUpdates(chatId ec.Id, date int64) ([]*em.Message, error) {
	var mess []*em.ShortMes

	if err := mr.getCollection().Find(bson.M{ "chatid.value": chatId.Value, "date": bson.M{ "$gt": date } }).Sort("-date").All(&mess); err != nil {
		return nil, errors.New("messages not found")
	}

	var messages []*em.Message
	for _, mes := range mess {
		m := fm.MessageFromShort(mes)
		m = mr.fillMessages(m, mes.Messages)
		messages = append(messages, m)
	}

	return messages, nil
}

func (mr *MessageRepo) Add(mes *em.ShortMes) (bool, error) {
	if mr.IsExist(mes.Id) {
		return false, errors.New("message already exist")
	}

	if err := mr.getCollection().Insert(&mes); err != nil {
		return false, err
	}

	return true, nil
}

func (mr *MessageRepo) Save(mes *em.ShortMes) (bool, error) {
	if !mr.IsExist(mes.Id) {
		return false, errors.New("mes not found")
	}

	if err := mr.getCollection().Update(bson.M{ "id.value": mes.Id.Value }, mes); err != nil {
		return false, errors.New("mes not found")
	}

	return true, nil
}

func (mr *MessageRepo) Remove(id em.Id) (bool, error) {
	if !mr.IsExist(id) {
		return false, errors.New("message not found")
	}

	if err := mr.getCollection().Remove(bson.M{ "id.value": id.Value }); err != nil {
		return false, errors.New("message not found")
	}

	return true, nil
}

func (mr *MessageRepo) IsExist(id em.Id) (bool) {
	_, err := mr.Get(id)
	return err == nil
}

func (mr *MessageRepo) Clean() {
	mr.session.Close()
}

// private

func (mr *MessageRepo) getCollection() (*mgo.Collection) {
	return mr.session.DB(configs.Default().Database()).C(configs.MessageCollection)
}

func (mr *MessageRepo) fillMessages(m *em.Message, ids []*em.Id) (*em.Message) {
	if len(ids) > 0 {
		for _, id := range ids {
			mes, err := mr.Get(*id)

			if err != nil {
				continue
			}

			m.Messages = append(m.Messages, mes)
		}
	}
	return m
}