package log

import (
	"gopkg.in/mgo.v2"
	"configs"
	"gopkg.in/mgo.v2/bson"
	"errors"
	el "entities/log"
)

type LogRepo struct {
	session *mgo.Session
}

// static

func New() (*LogRepo, error) {
	repo := &LogRepo{}
	session, err := mgo.Dial(configs.DBAddress)
	if err == nil {
		repo.session = session
	}
	return repo, err
}

// public

func (lr *LogRepo) Get(id el.Id) (*el.Log, error) {
	us := &el.Log{}

	if err := lr.getCollection().Find(bson.M{ "id.value": id.Value }).One(&us); err != nil {
		return nil, errors.New("log not found")
	}

	return us, nil
}

func (lr *LogRepo) Add(log *el.Log) (bool, error) {
	if lr.IsExist(log.Id) {
		return false, errors.New("log already exist")
	}

	if err := lr.getCollection().Insert(&log); err != nil {
		return false, err
	}

	return true, nil
}

func (lr *LogRepo) Save(log *el.Log) (bool, error) {
	if !lr.IsExist(log.Id) {
		return false, errors.New("log not found")
	}

	if err := lr.getCollection().Update(bson.M{ "id.value": log.Id.Value }, log); err != nil {
		return false, errors.New("log not found")
	}

	return true, nil
}

func (lr *LogRepo) Remove(id el.Id) (bool, error) {
	if !lr.IsExist(id) {
		return false, errors.New("log not found")
	}

	if err := lr.getCollection().Remove(bson.M{ "id.value": id.Value }); err != nil {
		return false, errors.New("log not found")
	}

	return true, nil
}

func (lr *LogRepo) IsExist(id el.Id) (bool) {
	_, err := lr.Get(id)
	return err == nil
}

func (lr *LogRepo) Clean() {
	lr.session.Close()
}

// private

func (lr *LogRepo) getCollection() (*mgo.Collection) {
	return lr.session.DB(configs.Default().Database()).C(configs.LogCollection)
}