package repositories_code

import (
	e "entities"
	"gopkg.in/mgo.v2"
	"configs"
	"gopkg.in/mgo.v2/bson"
	"errors"
)

type CodeRepo struct {
	session *mgo.Session
}

// static

func New() (*CodeRepo, error) {
	repo := &CodeRepo{}
	session, err := mgo.Dial(configs.DBAddress)
	if err == nil {
		repo.session = session
	}
	return repo, err
}

// public

func (cr *CodeRepo) Get(phone string) (*e.Code, error) {
	code := &e.Code{}

	if err := cr.getCollection().Find(bson.M{ "phone": phone }).One(&code); err != nil {
		return nil, errors.New("code not found")
	}

	return code, nil
}

func (cr *CodeRepo) Add(code *e.Code) (bool, error) {
	if cr.IsExist(code.Phone) {
		return false, errors.New("code already exist")
	}

	if err := cr.getCollection().Insert(&code); err != nil {
		return false, err
	}

	return true, nil
}

func (cr *CodeRepo) Save(code *e.Code) (bool, error) {
	if !cr.IsExist(code.Phone) {
		return false, errors.New("code not found")
	}

	if err := cr.getCollection().Update(bson.M{ "phone": code.Phone }, code); err != nil {
		return false, errors.New("code not found")
	}

	return true, nil
}

func (cr *CodeRepo) Remove(phone string) (bool, error) {
	if !cr.IsExist(phone) {
		return false, errors.New("code not found")
	}

	if err := cr.getCollection().Remove(bson.M{ "phone": phone }); err != nil {
		return false, errors.New("code not found")
	}

	return true, nil
}

func (cr *CodeRepo) IsExist(phone string) (bool) {
	_, err := cr.Get(phone)
	return err == nil
}

func (cr *CodeRepo) Clean() {
	cr.session.Close()
}

// private

func (cr *CodeRepo) getCollection() (*mgo.Collection) {
	return cr.session.DB(configs.Default().Database()).C(configs.CodeCollection)
}