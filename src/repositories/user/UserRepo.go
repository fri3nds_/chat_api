package repositories_user

import (
	"gopkg.in/mgo.v2"
	"configs"
	"gopkg.in/mgo.v2/bson"
	"errors"
	e "entities"
	eu "entities/user"
)

type UserRepo struct {
	session *mgo.Session
}

// static

func New() (*UserRepo, error) {
	repo := &UserRepo{}
	session, err := mgo.Dial(configs.DBAddress)
	if err == nil {
		repo.session = session
	}
	return repo, err
}

// public

func (ur *UserRepo) Get(id eu.Id) (*eu.User, error) {
	us := &eu.User{}

	if err := ur.getCollection().Find(bson.M{ "id.value": id.Value }).One(&us); err != nil {
		return nil, errors.New("user not found")
	}

	return us, nil
}

func (ur *UserRepo) GetByIds(ids []*eu.Id) ([]*eu.User, error) {
	var users []*eu.User

	if err := ur.getCollection().Find(bson.M{ "id.value": bson.M{ "$in": ur.stringIds(ids) } }).All(&users); err != nil {
		return nil, errors.New("users not found")
	}

	return users, nil
}

func (ur *UserRepo) GetByPhone(number e.Phone) (*eu.User, error) {
	us := &eu.User{}

	if err := ur.getCollection().Find(bson.M{ "phone.number": number.Number }).One(&us); err != nil {
		return nil, errors.New("user not found")
	}

	return us, nil
}

func (ur *UserRepo) GetByToken(token string) (*eu.User, error) {
	us := &eu.User{}

	if err := ur.getCollection().Find(bson.M{ "iostokens." + token: bson.M{ "$exists": true } }).One(&us); err != nil {
		return nil, errors.New("user not found")
	}

	return us, nil
}

func (ur *UserRepo) GetByUsername(username string) (*eu.User, error) {
	user := &eu.User{}

	if err := ur.getCollection().Find(bson.M{ "username": username }).One(&user); err != nil {
		return nil, errors.New("user not found")
	}

	return user, nil
}

func (ur *UserRepo) GetByPhones(phones []*e.Phone) ([]*eu.User, error) {
	var users []*eu.User

	if err := ur.getCollection().Find(bson.M{ "phone.number": bson.M{ "$in": ur.getNumbers(phones) } }).All(&users); err != nil {
		return nil, errors.New("users not found")
	}

	return users, nil
}

func (ur *UserRepo) SearchByUsername(username string) ([]*eu.User, error) {
	var users []*eu.User

	if err := ur.getCollection().Find(bson.M{ "username": bson.RegEx{ username + ".*", "" } }).All(&users); err != nil {
		return nil, errors.New("users not found")
	}

	return users, nil
}

func (ur *UserRepo) Add(user *eu.User) (bool, error) {
	if ur.IsExist(user.Id) {
		return false, errors.New("user already exist")
	}

	if err := ur.getCollection().Insert(&user); err != nil {
		return false, err
	}

	return true, nil
}

func (ur *UserRepo) Save(user *eu.User) (bool, error) {
	if !ur.IsExist(user.Id) {
		return false, errors.New("user not found")
	}

	if err := ur.getCollection().Update(bson.M{ "id.value": user.Id.Value }, user); err != nil {
		return false, errors.New("user not found")
	}

	return true, nil
}

func (ur *UserRepo) Remove(id eu.Id) (bool, error) {
	if !ur.IsExist(id) {
		return false, errors.New("user not found")
	}

	if err := ur.getCollection().Remove(bson.M{ "id.value": id.Value }); err != nil {
		return false, errors.New("user not found")
	}

	return true, nil
}

func (ur *UserRepo) IsExist(id eu.Id) (bool) {
	_, err := ur.Get(id)
	return err == nil
}

func (ur *UserRepo) IsExistPhone(phone e.Phone) (bool) {
	_, err := ur.GetByPhone(phone)
	return err == nil
}

func (ur *UserRepo) IsExistUsername(username string) (bool) {
	_, err := ur.GetByUsername(username)
	return err == nil
}

func (ur *UserRepo) Clean() {
	ur.session.Close()
}

// private

func (ur *UserRepo) getCollection() (*mgo.Collection) {
	return ur.session.DB(configs.Default().Database()).C(configs.UserCollection)
}

func (ur *UserRepo) getNumbers(phones []*e.Phone) ([]string) {
	var res []string
	for _, phone := range phones {
		res = append(res, phone.Number)
	}
	return res
}

func (ur *UserRepo) stringIds(ids []*eu.Id) ([]string) {
	var arr []string
	for _, id := range ids {
		arr = append(arr, id.Value)
	}
	return arr
}
