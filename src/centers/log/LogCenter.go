package centers_log

import (
	rl "repositories/log"
	el "entities/log"
)

// static

var Default = &LogCenter {}

// public

type LogCenter struct {
	repo *rl.LogRepo
}

func (lc *LogCenter) Init() {
	repo, err := rl.New()
	if err == nil {
		lc.repo = repo
	}
}

func (lc *LogCenter) Add(log *el.Log) (bool) {
	res, _ := lc.repo.Add(log)
	return res
}