package centers_observe

import (
	e "entities"
	ec "entities/chat"
	eu "entities/user"
	"errors"
	"sync"
)

// chats

func (oc *ObserveCenter) DialogDidAdded(name string, userInfo interface{}, object interface{}) {
	data, ok := userInfo.(map[string]interface{})
	if !ok {
		panic(errors.New("user info is not a map"))
	}

	dialog, ok := data["chat"].(*ec.Chat)
	if !ok {
		panic("dialog not found")
	}

	users, err := oc.userRepo.GetByIds(dialog.Users)
	if err != nil {
		panic("dialog not found")
	}

	devToken, _ := data["token"].(string)
	oc.sendChatToUsers(name, users, dialog, devToken)
}

func (oc *ObserveCenter) LastDidUpdate(name string, userInfo interface{}, object interface{}) {
	data, ok := userInfo.(map[string]interface{})
	if !ok {
		panic(errors.New("user info is not a map"))
	}

	last, ok := data["last"].(*ec.LastUpdate)
	if !ok {
		panic(errors.New("last not found"))
	}

	user, err := oc.userRepo.Get(*last.UserId)
	if err != nil {
		panic(err)
	}
	user.SubBadge(last.BadgeCount)
	last.BadgeCount = 0

	devToken, _ := data["token"].(string)
	oc.sendLastToUser(name, user, last, devToken)

	// save badge values
	oc.userRepo.Save(user)
	oc.lastRepo.Save(last)
}

func (oc *ObserveCenter) LastDidUpdateFriend(name string, userInfo interface{}, object interface{}) {
	data, ok := userInfo.(map[string]interface{})
	if !ok {
		panic(errors.New("user info is not a map"))
	}

	last, ok := data["last"].(*ec.LastUpdate)
	if !ok {
		panic(errors.New("last not found"))
	}

	chat, err := oc.chatRepo.Get(*last.ChatId)
	if err != nil {
		panic(err)
	}

	users, err := oc.userRepo.GetByIds(chat.Users)
	if err != nil {
		panic(err)
	}

	oc.sendLastToUsers(name, users, last)
}

// private

func (oc *ObserveCenter) sendChatToUsers(name string, users []*eu.User, chat *ec.Chat, devToken string) {
	var wg sync.WaitGroup
	defer wg.Wait()

	info := make(map[string]interface{})
	info["name"] = name
	info["chat"] = chat.Map()

	for _, us := range users {
		sendToUser := func(user *eu.User) {
			for tok := range user.IOSTokens {
				if tok != devToken {
					oc.send(tok, name, info, "New chat", chat.Title, -1)
				}
			}
			wg.Done()
		}

		wg.Add(1)
		go sendToUser(us)
	}
}

func (oc *ObserveCenter) sendLastToUser(name string, user *eu.User, last *ec.LastUpdate, devToken string) {
	var wg sync.WaitGroup
	defer wg.Wait()

	info := make(map[string]interface{})
	info["name"] = name
	info["last"] = last.Map()

	sendToUser := func (user *eu.User) {
		for tok, status := range user.IOSTokens {
			if tok != devToken && status != e.Offline {
				oc.send(tok, name, info, "", "", user.BadgeCount)
			}
		}
		wg.Done()
	}

	wg.Add(1)
	go sendToUser(user)
}

func (oc *ObserveCenter) sendLastToUsers(name string, users []*eu.User, last *ec.LastUpdate) {
	var wg sync.WaitGroup
	defer wg.Wait()

	info := make(map[string]interface{})
	info["name"] = name
	info["last"] = last.Map()

	for _, user := range users {
		if user.Id.Value != last.UserId.Value {
			sendToUser := func(user *eu.User) {
				for tok, status := range user.IOSTokens {
					if status != e.Offline {
						oc.send(tok, name, info, "", "", user.BadgeCount)
					}
				}
				wg.Done()
			}

			wg.Add(1)
			go sendToUser(user)
		}
	}
}