package centers_observe

import (
	ru "repositories/user"
)

const (
	TokenInvalid = "TokenInvalid"
)

type TokenObserver struct {
	userRepo *ru.UserRepo
}

// static part

var observer *TokenObserver

func DefaultTokenObserver() (*TokenObserver) {
	if observer != nil {
		return observer
	}

	repo, err := ru.New()
	if err != nil {
		panic(err)
	}

	observer = &TokenObserver { userRepo: repo }
	return observer
}

// public part

func (to *TokenObserver) TokenInvalid(name string, userInfo interface{}, object interface{}) {
	token := userInfo.(map[string]interface{})["token"].(string)
	user, err := to.userRepo.GetByToken(token)
	for err == nil {
		user.DelToken(token)
		to.userRepo.Save(user)
		user, err = to.userRepo.GetByToken(token)
	}
}
