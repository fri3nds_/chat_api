package centers_observe

import (
	cp "centers/push"
	"configs"
	ec "entities/chat"
	eu "entities/user"
	"fmt"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/payload"
	"github.com/sideshow/apns2/token"
	rc "repositories/chat"
	rm "repositories/message"
	ru "repositories/user"
)

const (
	DialogDidAdded 		= "DialogDidAdded"
	MessageDidAdded 	= "MessageDidAdded"
	LastDidUpdate		= "LastDidUpdate"
	LastDidUpdateFriend = "LastDidUpdateFriend"
)

type ObserveCenter struct {
	userRepo 	*ru.UserRepo
	chatRepo 	*rc.ChatRepo
	lastRepo	*rc.LastUpdateRepo
	messageRepo *rm.MessageRepo
}

// static

func newCenter() (*ObserveCenter) {
	userRepo, err := ru.New()
	if err != nil {
		panic(err)
	}

	chatRepo, err := rc.New()
	if err != nil {
		panic(err)
	}

	lastRepo, err := rc.NewLast()
	if err != nil {
		panic(err)
	}

	messageRepo, err := rm.New()
	if err != nil {
		panic(err)
	}

	return &ObserveCenter {
		userRepo: userRepo,
		chatRepo: chatRepo,
		lastRepo: lastRepo,
		messageRepo: messageRepo,
	}
}

var center *ObserveCenter

func Default() (*ObserveCenter) {
	if center != nil {
		return center
	}
	center = newCenter()
	return center
}

// main method

func (oc *ObserveCenter) send(deviceToken, name string, userInfo map[string]interface{}, alert, alertBody string, badgeCount int32) {
	notif := oc.getNotif(deviceToken)
	notif.Payload = oc.preparePayload(name, userInfo, alert, alertBody, badgeCount)

	client := apns2.NewTokenClient(oc.getToken())

	if configs.Default().IsProduction() {
		client = client.Production()
	}

	res, err := client.Push(notif)

	if err != nil {
		panic(err)
	}

	if res.Sent() {
		fmt.Println("Sent:", res.ApnsID)
	} else {
		m := make(map[string]interface{})
		m["token"] = deviceToken
		cp.Default().Push(TokenInvalid, nil, m)

		fmt.Println("Not sent %v %v %v\n", res.StatusCode, res.ApnsID, res.Reason)
	}
}

// helps

func (oc *ObserveCenter) getCurr(id *eu.Id, users *[]*eu.User) (*eu.User) {
	var curr *eu.User
	for _, us := range *users {
		if us.Id.Value == id.Value {
			curr = us
			break
		}
	}
	return curr
}

func (oc *ObserveCenter) getToken() (*token.Token) {
	authKey, err := token.AuthKeyFromFile("cert/AuthKey_QKPQBC9MF5.p8")

	if err != nil {
		panic(err)
	}

	tok := &token.Token {
		AuthKey: authKey,
		KeyID: "QKPQBC9MF5",
		TeamID: "EA23S8ZKCR",
	}

	return tok
}

func (oc *ObserveCenter) getNotif(deviceToken string) (*apns2.Notification) {
	return &apns2.Notification {
		DeviceToken: deviceToken,
		Topic: "com.nvk3d.Simple",
	}
}

func (oc *ObserveCenter) preparePayload(name string, userInfo map[string]interface{}, alert, alertBody string, badgeCount int32) (*payload.Payload) {
	pay := payload.NewPayload()
	pay.AlertTitle(alert)
	pay.AlertBody(alertBody)
	if badgeCount >= 0 {
		pay.Badge(int(badgeCount))
	}
	pay.Sound("default")
	pay.ContentAvailable()
	pay.Custom("name", name)

	for k, v := range userInfo {
		pay.Custom(k, v)
	}

	return pay
}

func (oc *ObserveCenter) saveUsers(users []*eu.User) {
	for _, user := range users {
		oc.userRepo.Save(user)
	}
}

func (oc *ObserveCenter) saveLasts(lasts []*ec.LastUpdate) {
	for _, last := range lasts {
		res, _ := oc.lastRepo.Save(last)
		if !res {
			oc.lastRepo.Add(last)
		}
	}
}