package centers_observe

import (
	ec "entities/chat"
	em "entities/message"
	eu "entities/user"
	"errors"
	"factories/chat"
	"sync"
)

// messages

func (oc *ObserveCenter) MessageDidAdded(name string, userInfo interface{}, object interface{}) {
	data, ok := userInfo.(map[string]interface{})
	if !ok {
		panic(errors.New("user info is not a map"))
	}

	message, ok := data["message"].(*em.ShortMes)
	if !ok {
		panic(errors.New("message not found"))
	}

	chat, err := oc.chatRepo.Get(*message.ChatId)
	if err != nil {
		panic(err)
	}

	users, err := oc.userRepo.GetByIds(chat.Users)
	if err != nil {
		panic(err)
	}
	oc.incBadgeUsers(1, users, message.UserId)
	lasts := oc.lastsByUsers(message.ChatId, users, message.UserId)
	oc.incBadgeLasts(1, lasts)

	devToken, _ := data["token"].(string)
	oc.sendMessageToUsers(name, users, message, devToken)

	// save badge values
	oc.saveUsers(users)
	oc.saveLasts(lasts)
}

// private

func (oc *ObserveCenter) sendMessageToUsers(name string, users []*eu.User, message *em.ShortMes, devToken string) {
	var wg sync.WaitGroup
	defer wg.Wait()

	info := make(map[string]interface{})
	info["name"] = name
	info["message"] = message.Map()

	curr := oc.getCurr(message.UserId, &users)
	for _, us := range users {
		sendToUser := func (user *eu.User) {
			for tok, status := range user.IOSTokens {
				if tok != devToken {
					if user.Id.Value != message.UserId.Value || status == 1 {
						if curr.Name != "" {
							oc.send(tok, name, info, curr.Name, message.Text, user.BadgeCount)
						} else {
							oc.send(tok, name, info, "New message", message.Text, user.BadgeCount)
						}
					}
				}
			}
			wg.Done()
		}

		wg.Add(1)
		go sendToUser(us)
	}
}

func (oc *ObserveCenter) incBadgeUsers(value int32, users []*eu.User, currentUserId *eu.Id) {
	for _, user := range users {
		if user.Id.Value != currentUserId.Value {
			user.AddBadge(value)
		}
	}
}

func (oc *ObserveCenter) incBadgeLasts(value int32, lasts []*ec.LastUpdate) {
	for _, last := range lasts {
		last.BadgeCount += value
	}
}

func (oc *ObserveCenter) lastsByUsers(chatId *ec.Id, users []*eu.User, currentUserId *eu.Id) ([]*ec.LastUpdate) {
	lasts := []*ec.LastUpdate{}
	for _, user := range users {
		if user.Id.Value == currentUserId.Value {
			continue
		}

		last, _ := oc.lastRepo.Get(chatId, &user.Id)
		if last != nil {
			lasts = append(lasts, last)
		} else {
			lasts = append(lasts, factories_chat.NewLast(chatId, &user.Id, 0))
		}
	}
	return lasts
}