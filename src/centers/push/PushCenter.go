package centers_push

import (
	"errors"
	"reflect"
	"sync"
)

type PushCenter struct {
	events 			map[string][]map[string]interface{}
}

// static

var pc *PushCenter

func Default() (*PushCenter) {
	if pc != nil {
		return pc
	}
	pc = &PushCenter { events: make(map[string][]map[string]interface{}) }
	return pc
}

// public

func (pc *PushCenter) Push(name string, object interface {}, userInfo map[string]interface{}) {
	var wg sync.WaitGroup
	defer wg.Wait()

	if pc.events[name] != nil {
		for _, elem := range pc.events[name] {
			if reflect.DeepEqual(elem["object"], object) {
				wg.Add(1)
				go pc.call(&wg, elem, "method", name, userInfo, object)
			}
		}
	}
}

func (pc *PushCenter) AddObserver(method func(name string, object interface{}, userInfo interface{}), name string, object interface{}) {
	params := make(map[string]interface {})
	params["method"] = method
	params["object"] = object
	pc.events[name] = append(pc.events[name], params)
}

func (pc *PushCenter) RemoveObserver(name string, object interface {}) {
	if pc.events[name] != nil {
		for i, rCount, rLen := 0, 0, len(pc.events[name]); i < rLen; i++ {
			j := i - rCount
			elem := pc.events[name][j]

			if reflect.DeepEqual(elem["object"], object) {
				pc.events[name] = append(pc.events[name][:j], pc.events[name][j+1:]...)
				rCount++
			}
		}
	}
}

// private

func (pc *PushCenter) call(wg *sync.WaitGroup, m map[string]interface{}, name string, params ... interface{}) (error) {
	defer wg.Done()

	f := reflect.ValueOf(m[name])
	funcType := reflect.TypeOf(m[name])

	if len(params) != f.Type().NumIn() {
		return errors.New("params num is not adapted")
	}

	in := make([]reflect.Value, len(params))
	for i, param := range params {
		expectedType := funcType.In(i)

		if param == nil {
			in[i] = reflect.New(expectedType).Elem()
		} else {
			in[i] = reflect.ValueOf(param)
		}
	}

	f.Call(in)
	return nil
}
