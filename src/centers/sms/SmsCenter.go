package centers_sms

import (
	e "entities"
	"net/http"
	bytes2 "bytes"
	"io/ioutil"
	"fmt"
	cl "centers/log"
	fl "factories/log"
	"strconv"
)

const smsAeroUrl = "https://nike_bondar@mail.ru:2UPtcyVUTHDVT1TUVF6CkTkV4aMT@gate.smsaero.ru/v2/"

type SmsCenter struct {}

// public

func (sc *SmsCenter) Send(phone *e.Phone, code *e.Code) {
	params := map[string]string {
		"number": 	phone.Number,
		"text": 	"Simple code " + code.Value,
		"sign": 	"SMS Aero",
		"channel": 	"DIRECT",
	}

	sc.do("POST", smsAeroUrl + "sms/send", params)
}

// private

func (sc *SmsCenter) do(method string, url string, params map[string]string) {
	json := makeJson(params)

	request, err := http.NewRequest(method, url, bytes2.NewBuffer([]byte(json)))
	request.Header.Set("Content-Type", "application/json")

	if err != nil {
		panic(err)
	}

	client := http.Client{}
	resp, err := client.Do(request)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	// for logs

	if resp.StatusCode != http.StatusOK {
		bytes, _ := ioutil.ReadAll(resp.Body)
		cl.Default.Add(fl.New(strconv.Itoa(resp.StatusCode), fmt.Sprintf("%s", string(bytes))))
	}
}

func makeJson(params map[string]string) ([]byte) {
	body := `{`
	i := 0
	for k, v := range params {
		body += `"` + k + `":` + `"` + v + `"`
		if i != len(params) - 1 {
			body += `,`
		}
		i += 1
	}
	body += `}`
	return []byte(body)
}