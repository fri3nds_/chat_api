package centers_chat

import (
	co "centers/observe"
	cp "centers/push"
	ec "entities/chat"
	eu "entities/user"
	fc "factories/chat"
	"help"
	rc "repositories/chat"
	ru "repositories/user"
	"time"
)

type ChatCenter struct {
	chatRepo *rc.ChatRepo
	lastRepo *rc.LastUpdateRepo
	userRepo *ru.UserRepo
}

// static

func New() (*ChatCenter, error) {
	chatRepo, err := rc.New()
	if err != nil {
		return nil, err
	}

	lastRepo, err := rc.NewLast()
	if err != nil {
		return nil, err
	}

	userRepo, err := ru.New()
	if err != nil {
		return nil, err
	}

	return &ChatCenter { chatRepo, lastRepo, userRepo }, nil
}

// public

func (cc *ChatCenter) GetChat(id *ec.Id, currUserId *eu.Id) (*ec.Chat) {
	ch, err := cc.chatRepo.Get(*id)

	if err != nil {
		return nil
	}

	if ch.Type == ec.Dialog {
		ch.Title = cc.titleForDialog(ch, currUserId)
	}

	last, _ := cc.lastRepo.Get(&ch.Id, currUserId)
	ch.SetLastUpdate(last)

	lastFriend, _ := cc.lastRepo.GetLastFriend(&ch.Id, currUserId)
	ch.SetLastUpdateFriend(lastFriend)

	return ch
}

func (cc *ChatCenter) GetChatWith(friendId *eu.Id, id *eu.Id) (*ec.Chat) {
	ch, err := cc.chatRepo.GetWith(*friendId, *id)

	if err != nil {
		return nil
	}

	ch.Title = cc.titleForDialog(ch, id)

	last, _ := cc.lastRepo.Get(&ch.Id, id)
	ch.SetLastUpdate(last)

	lastFriend, _ := cc.lastRepo.GetLastFriend(&ch.Id, id)
	ch.SetLastUpdateFriend(lastFriend)

	return ch
}

func (cc *ChatCenter) GetChats(id *eu.Id) ([]*ec.Chat) {
	chs, err := cc.chatRepo.GetByUser(*id)

	if err != nil {
		return nil
	}

	for _, ch := range chs {
		if ch.Type == ec.Dialog {
			ch.Title = cc.titleForDialog(ch, id)
		}

		last, _ := cc.lastRepo.Get(&ch.Id, id)
		ch.SetLastUpdate(last)

		lastFriend, _ := cc.lastRepo.GetLastFriend(&ch.Id, id)
		ch.SetLastUpdateFriend(lastFriend)
	}

	return chs
}

func (cc *ChatCenter) AddDialog(id *eu.Id, sid *eu.Id, devToken string) (*ec.Chat) {
	ch, _ := cc.chatRepo.GetWith(*sid, *id)
	if ch != nil {
		return nil
	}

	ch = fc.NewDialog(id, sid)

	if _, err := cc.chatRepo.Add(ch); err != nil {
		return nil
	}

	ch.SetLastUpdate(nil)
	ch.SetLastUpdateFriend(nil)

	cp.Default().Push(co.DialogDidAdded, nil, cc.prepareForPush(ch, devToken))
	return ch
}

func (cc *ChatCenter) AddGroup(title string, adminId *eu.Id, ids []*eu.Id,) (*ec.Chat) {
	ch := fc.NewGroup(title, adminId, ids)

	if _, err := cc.chatRepo.Add(ch); err != nil {
		return nil
	}

	return ch
}

func (cc *ChatCenter) AddChannel(title string, adminId *eu.Id) (*ec.Chat) {
	ch := fc.NewChannel(title, adminId)

	if _, err := cc.chatRepo.Add(ch); err != nil {
		return nil
	}

	return ch
}

func (cc *ChatCenter) EditChat(id *ec.Id, title string) (bool) {
	ch, err := cc.chatRepo.Get(*id)

	if err != nil {
		return false
	}

	ch.Title = title
	res, _ := cc.chatRepo.Save(ch)
	return res
}

func (cc *ChatCenter) AddUser(id *ec.Id, userId *eu.Id) (bool) {
	ch, err := cc.chatRepo.Get(*id)

	if err != nil {
		return false
	}

	if help.Contains(ch.Users, userId) {
		return false
	}

	ch.Users = append(ch.Users, userId)
	res, _ := cc.chatRepo.Save(ch)
	return res
}

func (cc *ChatCenter) DelUser(id *ec.Id, adminId *eu.Id, delId *eu.Id) (bool) {
	ch, err := cc.chatRepo.Get(*id)

	if err != nil {
		return false
	}

	if ch.Type == ec.Dialog || !help.Contains(ch.Admins, adminId) {
		return false
	}

	help.Remove(&ch.Users, delId)
	res, _ := cc.chatRepo.Save(ch)
	return res
}

func (cc *ChatCenter) UpdateLast(chatId *ec.Id, userId *eu.Id, devToken string) (bool, int64) {
	date := time.Now().UnixNano()
	last, _ := cc.lastRepo.Get(chatId, userId)
	res := false

	if last != nil {
		if last.Date < date {
			last.Date = date
			res, _ = cc.lastRepo.Save(last)
		}
	} else {
		last = fc.NewLast(chatId, userId, date)
		res, _ = cc.lastRepo.Add(last)
	}

	// send push
	cp.Default().Push(co.LastDidUpdate, nil, cc.prepareForPushLast(last, devToken))
	cp.Default().Push(co.LastDidUpdateFriend, nil, cc.prepareForPushLast(last, devToken))

	return res, date
}

// helps

func (cc *ChatCenter) titleForDialog(ch *ec.Chat, currId *eu.Id) (string) {
	var usId *eu.Id

	if ch.Users[0].Value == currId.Value {
		usId = ch.Users[1]
	} else {
		usId = ch.Users[0]
	}

	us, err := cc.userRepo.Get(*usId)

	if err != nil {
		return ""
	}

	return us.Name
}

func (cc *ChatCenter) prepareForPush(ch *ec.Chat, devToken string) (map[string]interface{}) {
	mapp := make(map[string]interface{})
	mapp["chat"] = ch
	mapp["token"] = devToken
	return mapp
}

func (cc *ChatCenter) prepareForPushLast(last *ec.LastUpdate, devToken string) (map[string]interface{}) {
	mapp := make(map[string]interface{})
	mapp["last"] = last
	mapp["token"] = devToken
	return mapp
}
