package centers_auth

import (
	re "repositories/user"
	e "entities"
	fu "factories/user"
	fc "factories/code"
	rc "repositories/code"
	eu "entities/user"
	"help"
	cs "centers/sms"
	"golang.org/x/crypto/bcrypt"
)

type AuthCenter struct {
	usRepo 		*re.UserRepo
	codeRepo 	*rc.CodeRepo
	smsCenter 	*cs.SmsCenter
}

// static

func New() (*AuthCenter, error) {
	usRepo, err := re.New()
	if err != nil {
		return nil, err
	}
	codeRepo, err := rc.New()
	if err != nil {
		return nil, err
	}
	smsCenter := &cs.SmsCenter {}
	return &AuthCenter{ usRepo, codeRepo, smsCenter }, nil
}

// public

func (ac *AuthCenter) Login(phone *e.Phone) (bool) {
	if !ac.usRepo.IsExistPhone(*phone) {
		us := fu.UserWithPhone(*phone)
		if res, _ := ac.usRepo.Add(us); !res {
			return false
		}
	}

	// remove old code
	ac.codeRepo.Remove(phone.Number)

	// generate code
	code := fc.New(*phone)
	if res, _ := ac.codeRepo.Add(code); !res {
		return false
	}

	// send sms
	ac.smsCenter.Send(phone, code)

	return true
}

func (ac *AuthCenter) Verify(phone *e.Phone, code string) (*eu.User) {
	if ac.codeRepo.IsExist(phone.Number) {
		c, _ := ac.codeRepo.Get(phone.Number)
		if c.Valid() && c.Value == code {
			us, _ := ac.usRepo.GetByPhone(*phone)
			ac.codeRepo.Remove(phone.Number)
			return us
		}
	}
	return nil
}

func (ac *AuthCenter) LoginWithoutPhone(username, password string) (*eu.User) {
	if ac.usRepo.IsExistUsername(username) {
		us, err := ac.usRepo.GetByUsername(username)
		if err != nil {
			return nil
		}
		if err := bcrypt.CompareHashAndPassword([]byte(us.Password), []byte(password)); err != nil {
			return nil
		}
		return us
	}
	return nil
}

func (ac *AuthCenter) Register(username, password string) (bool) {
	if !ac.usRepo.IsExistUsername(username) {
		pass, _ := bcrypt.GenerateFromPassword([]byte(password), 1)
		us := fu.UserWithUsernameAndPassword(username, string(pass))
		res, _ := ac.usRepo.Add(us)
		return res
	}
	return false
}

func (ac *AuthCenter) Logout(devToken string) (bool) {
	if us, _ := ac.usRepo.GetByToken(devToken); us != nil {
		us.DelToken(devToken)
		ac.usRepo.Save(us)
	}
	return true
}

func (ac *AuthCenter) Refresh(id *eu.Id) (string, string) {
	us, _ := ac.usRepo.Get(*id)
	return help.GenerateJwtTokens(us)
}

// system methods

func (ac *AuthCenter) Clean() {
	ac.usRepo.Clean()
	ac.codeRepo.Clean()
}
