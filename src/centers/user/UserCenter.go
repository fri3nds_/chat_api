package centers_user

import (
	"golang.org/x/crypto/bcrypt"
	ru "repositories/user"
	e "entities"
	eu "entities/user"
)

type UserCenter struct {
	userRepo *ru.UserRepo
}

// static

func New() (*UserCenter, error) {
	userRepo, err := ru.New()
	if err != nil {
		return nil, err
	}
	return &UserCenter { userRepo }, nil
}

// public

func (uc *UserCenter) Get(id *eu.Id) (*eu.User) {
	user, err := uc.userRepo.Get(*id)

	if err != nil {
		return nil
	}

	return user
}

func (uc *UserCenter) GetByPhone(phone *e.Phone) (*eu.User) {
	user, err := uc.userRepo.GetByPhone(*phone)

	if err != nil {
		return nil
	}

	return user
}

func (uc *UserCenter) GetByUsername(username string) (*eu.User) {
	user, err := uc.userRepo.GetByUsername(username)

	if err != nil {
		return nil
	}

	return user
}

func (uc *UserCenter) GetByPhones(phones []*e.Phone) ([]*eu.User) {
	users, err := uc.userRepo.GetByPhones(phones)

	if err != nil {
		return nil
	}

	return users
}

func (uc *UserCenter) EditName(name string, id *eu.Id) (bool) {
	us, err := uc.userRepo.Get(*id)

	if err != nil {
		return false
	}

	us.Name = name
	uc.userRepo.Save(us)
	return true
}

func (uc *UserCenter) SearchByUsername(username string) ([]*eu.User) {
	users, err := uc.userRepo.SearchByUsername(username)

	if err != nil {
		return nil
	}

	return users
}

func (uc *UserCenter) EditUsername(username string, id *eu.Id) (bool) {
	us, err := uc.userRepo.Get(*id)
	if err != nil {
		return false
	}

	usByUsername, errByUsername := uc.userRepo.GetByUsername(username)
	if errByUsername == nil && usByUsername.Id.Value != us.Id.Value {
		return false
	}

	us.Username = username
	uc.userRepo.Save(us)
	return true
}

func (uc *UserCenter) EditPhone(phone *e.Phone, id *eu.Id) (bool) {
	us, err := uc.userRepo.Get(*id)
	if err != nil {
		return false
	}

	usByPhone, errByPhone := uc.userRepo.GetByPhone(*phone)
	if errByPhone == nil && usByPhone.Id.Value != us.Id.Value {
		return false
	}

	us.Phone = *phone
	uc.userRepo.Save(us)
	return true
}

func (uc *UserCenter) EditPassword(oldPassword, newPassword string, id *eu.Id) (bool) {
	us, err := uc.userRepo.Get(*id)
	if err != nil {
		return false
	}

	if us.Password != "" {
		if err := bcrypt.CompareHashAndPassword([]byte(us.Password), []byte(oldPassword)); err != nil {
			return false
		}
	}

	pass, _ := bcrypt.GenerateFromPassword([]byte(newPassword), 1)
	us.Password = string(pass)
	res, _ := uc.userRepo.Save(us)
	return res
}

func (uc *UserCenter) SetStatus(status e.Status, token string, id *eu.Id) (bool) {
	user, err := uc.userRepo.Get(*id)

	if err != nil {
		return false
	}

	user.AddStatus(status, token)
	uc.userRepo.Save(user)
	return true
}

func (uc *UserCenter) AddToken(token string, id *eu.Id) (bool) {
	if token == "" {
		return false
	}

	user, err := uc.userRepo.Get(*id)
	if err != nil {
		return false
	}

	if us, _ := uc.userRepo.GetByToken(token); us != nil && user.Id.Value != us.Id.Value {
		us.DelToken(token)
		uc.userRepo.Save(us)
	}

	user.AddToken(token)
	uc.userRepo.Save(user)
	return true
}

func (uc *UserCenter) DelToken(token string, id *eu.Id) (bool) {
	user, err := uc.userRepo.Get(*id)

	if err != nil {
		return false
	}

	user.DelToken(token)
	uc.userRepo.Save(user)
	return true
}