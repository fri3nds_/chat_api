package centers_message

import (
	rm "repositories/message"
	em "entities/message"
	ec "entities/chat"
	eu "entities/user"
	fm "factories/message"
	cp "centers/push"
	co "centers/observe"
)

const pageSize = 30

type MessageCenter struct {
	messageRepo *rm.MessageRepo
}

// static

func New() (*MessageCenter, error) {
	messageRepo, err := rm.New()
	if err != nil {
		return nil, err
	}
	return &MessageCenter { messageRepo }, nil
}

// public

func (mc *MessageCenter) GetMessage(id *em.Id) (*em.Message) {
	m, err := mc.messageRepo.Get(*id)

	if err != nil {
		return nil
	}

	return m
}

func (mc *MessageCenter) GetMessages(chatId *ec.Id, page int) ([]*em.Message) {
	ms, err := mc.messageRepo.GetByChat(*chatId, page, pageSize)

	if err != nil {
		return nil
	}

	return ms
}

func (mc *MessageCenter) GetLastMessages(ids []*ec.Id) ([]*em.Message) {
	messages := []*em.Message{}
	for _, id := range ids {
		ms, err := mc.messageRepo.GetByChat(*id, 0, 1)

		if err != nil {
			continue
		}

		messages = append(messages, ms...)
	}
	return messages
}

func (mc *MessageCenter) GetUpdates(chatId *ec.Id, date int64) ([]*em.Message) {
	messages, err := mc.messageRepo.GetUpdates(*chatId, date)

	if err != nil {
		return nil
	}

	return messages
}

func (mc *MessageCenter) AddMessage(m *em.ShortMes, devToken string) (bool) {
	res, err := mc.messageRepo.Add(m)

	if err != nil {
		return false
	}

	cp.Default().Push(co.MessageDidAdded, nil, mc.prepareForPush(m, devToken))
	return res
}

func (mc *MessageCenter) EditMessage(id *eu.Id, m *em.ShortMes) (bool) {
	mes, err := mc.messageRepo.Get(m.Id)

	if err != nil {
		return false
	}

	if id.Value == mes.UserId.Value {
		res, _ := mc.messageRepo.Save(m)
		return res
	}
	return false
}

func (mc *MessageCenter) DelMessage(id *em.Id, userId *eu.Id) (bool) {
	m, err := mc.messageRepo.Get(*id)

	if err != nil {
		return false
	}

	res, _ := mc.messageRepo.Save(fm.ShortFromMessage(m))
	return res
}

// private

func (mc *MessageCenter) prepareForPush(m *em.ShortMes, devToken string) (map[string]interface{}) {
	mapp := make(map[string]interface{})
	mapp["message"] = m
	mapp["token"] = devToken
	return mapp
}