package validate

import (
	ru "repositories/user"
	"github.com/dgrijalva/jwt-go"
	"fmt"
	eu "entities/user"
	"time"
)

type ValidateCenter struct {
	userRepo *ru.UserRepo
}

// static

func New() (*ValidateCenter) {
	ur, err := ru.New()
	if err != nil {
		return nil
	}
	vc := &ValidateCenter {
		userRepo: ur,
	}
	return vc
}

// public

func (vc *ValidateCenter) ValidateJwt(token string) (bool) {
	if token == "" {
		return false
	}
	t, _ := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", t.Header["alg"])
		}

		id := t.Claims.(jwt.MapClaims)["id"].(string)
		if us, err := vc.userRepo.Get(eu.Id { Value: id } ); err == nil {
			return []byte(us.Secret), nil
		}
		return []byte(""), nil
	})

	if exp := int64(t.Claims.(jwt.MapClaims)["exp"].(float64)); exp < time.Now().Unix() {
		return false
	}

	if _, ok := t.Claims.(jwt.MapClaims); ok && t.Valid {
		return true
	}

	return false
}

func (vc *ValidateCenter) ValidateJwtId(token string) (*eu.Id, bool) {
	if token == "" {
		return nil, false
	}
	t, _ := jwt.Parse(token, func (t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", t.Header["alg"])
		}

		id := t.Claims.(jwt.MapClaims)["id"].(string)
		if us, err := vc.userRepo.Get(eu.Id { Value: id }); err == nil {
			return []byte(us.Secret), nil
		}
		return []byte(""), nil
	})

	if t.Valid {
		return &eu.Id{Value: t.Claims.(jwt.MapClaims)["id"].(string)}, true
	}
	return nil, false
}

