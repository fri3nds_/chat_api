#!/bin/bash

go get -u google.golang.org/grpc
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/satori/go.uuid
go get -u gopkg.in/mgo.v2
go get -u github.com/dgrijalva/jwt-go
go get -u github.com/sideshow/apns2
go get -u golang.org/x/crypto/bcrypt